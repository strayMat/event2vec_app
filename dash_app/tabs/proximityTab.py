import dash_html_components as html
import dash_core_components as dcc

from dataLoader import drop_down_choices, available_terminologies_choices
from copy import deepcopy

first_drop_down = dcc.Dropdown(
    id='query_concept1',
    options=drop_down_choices,
    # multi=True,
    value="J44",
    persistence=True
)

second_dropdown = dcc.Dropdown(
    options=drop_down_choices,
    id='query_concept2',
    value=[],
    multi=True,
    persistence=True,
    placeholder='Select a list of specific concepts against which to query proximity'
)

proximity_query_tab = html.Div([
    html.H4('Proximity between medical concepts'),
    html.P('Because we embedded the medical concepts in a common vector space, we can obtain valuable metrics such as cosine distance. '
           'Thus, for a given concept, we can query the closest medical embeddings in this space and compare it to the common knowledge in the SNDS context.'),
    html.Label('Vocabulary searched :'),
    dcc.Checklist(
        id='query_terminologies',
        options=available_terminologies_choices,
        value=['atc7', 'cim10', 'nabm', 'ngap', 'ccam'],
        labelStyle={'display': 'inline-block'}
    ),
    html.Label('Query concept 1 :'),
    first_drop_down,
    html.Label('Query concept 2 :'),
    second_dropdown,
    html.Div(
        id='output-proximity',
        children="Choose a concept..."
    )
    # html.Label('Query concept'),
    # dcc.Input(value='query_concept', type='text')
])
