import dash_html_components as html
import dash_core_components as dcc

from dataLoader import explanation_markdown

explanations_tab = html.Div([

    dcc.Markdown(explanation_markdown)
])
