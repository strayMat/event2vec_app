import dash_html_components as html
import dash_core_components as dcc
import plotly.graph_objs as go


from dataLoader import tsne_data, embedding_dimension, perplexity, niter

vocabulary_length = tsne_data.shape[0]
figure = go.Figure(
    layout=dict(
        #title="{}".format('snds2vec'),
        autosize=True,
        width=1000,
        height=800,
        xaxis={'title': 'x', 'showticklabels': True},
        yaxis={'title': 'y', 'showticklabels': False},
        margin={'l': 50, 'b': 0, 't': 0, 'r': 50},
        showlegend=True,
        hovermode='closest')
)
# plotted vocabularies
vocabs = ['atc7', 'ccam', 'cim10', 'nabm', 'ngap', 'unknown']
for vocab in vocabs:
    vocab_data = tsne_data.loc[tsne_data['concept_terminology'] == vocab, :]
    figure.add_trace(
    go.Scattergl(
        x=vocab_data['x'],
        y=vocab_data['y'],
        legendgroup="terminology",
        name=vocab,
        text=vocab_data['plotted_label'],
        mode='markers',
        opacity=0.8,
        marker={
            'size': 6,
            'color': vocab_data['plotted_color']
        })
    )

figure.update_layout(legend=dict(x=0, y=1))

tsne_plot_tab = html.Div([
    html.Div([
        html.H4('TSNE plot of the {} embedded medical concepts'.format(vocabulary_length)),
        html.H6('(dim={}, perplexity={}, n_iter={})'.format(
            embedding_dimension, perplexity, niter)),
        html.P('TSNE is a projection method widely used for high-dimensional data vizualisation.'),
        html.P('On this plot each dot is a medical concept. '
               'Colors represent the concept terminologies.'),
        html.P('Proximity between group of concepts clearly appears in the plot, reflecting the '
               'short term dependencies between the medical concepts of the SNDS.')

    ], className='two columns', style={'width': '20%', 'height': '100%'}),
    dcc.Graph(
        id='life-exp-vs-gdp',
        figure=figure,
        config={'scrollZoom': True},
        className='two columns',
        style={'width': "75%", 'height': '100%'}
    )],
    style={
        'width': '100%',
        'display': 'inline-block'})
