import dash_html_components as html

import pandas as pd
from copy import deepcopy
from sklearn.metrics.pairwise import cosine_similarity
from typing import List


def generate_table(dataframe: pd.DataFrame, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )


def concept_proximity(
        query_code: str,
        embeddings_df: pd.DataFrame,
        k: int = 1,
        target_terminologies: List[str] = ['']) -> pd.DataFrame:

    #available_terminologies = ['ccam', 'cim10', 'nabm', 'atc', '']
    #assert target_terminologies in available_terminologies 'target_terminologies can only be in available keys: {}'.format(available_terminologies)
    assert query_code in embeddings_df[
        'concept_code'].values, 'query_code should be in the embeddings dataframe'

    query_vector = embeddings_df.loc[
        embeddings_df['concept_code'] == query_code, 'concept_embedding'].values[0]

    concept_distances = deepcopy(embeddings_df)
    concept_distances['distance2query'] = cosine_similarity(
        [query_vector],
        list(concept_distances['concept_embedding'].values))[0]

    concept_distances.sort_values('distance2query', ascending=False, inplace=True)
    concept_distances = concept_distances.loc[concept_distances['concept_code'] != query_code, :]

    if target_terminologies != ['']:
        top_concepts = concept_distances.loc[
                       concept_distances['concept_terminology'].isin(target_terminologies), :][:k]
    else:
        top_concepts = concept_distances[:k]

    kept_cols = ['concept_code', 'concept_name', 'concept_terminology', 'distance2query']
    top_concepts_cleaned = top_concepts.loc[:, kept_cols]

    return top_concepts_cleaned
