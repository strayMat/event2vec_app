## Usage

We let the user explore medical embeddings built on french national health insurance database in two ways: 
- **TSNE plot** : a 2D projection of the embeddings where we can explore the continuous space of embeddings.  
- **Concept Proximity** : Query a distance between embedded concepts (takes ~15s to load because of query indexing). 

## Context and Data 

Medico-administrative databases are very rich sources of information on health care systems. However, their exploitation is delicate because of their complexity. We hope to leverage unsupervised approaches that have proven to be very successful in natural language processing by applying word2vec to care sequences. We propose rich vector representations that reflect the interactions (co-occurrences) during care pathways between the codes or events of four major French medical terminologies.
This method has already been adopted on mixed data types such as text, claims and pubmed articles by *Beam et al, (2018)*.  

The [SNDS](https://documentation-snds.health-data-hub.fr/introduction/) is the French national insurance claims data pulled with other medico-administrative databases such as the hospital EHRs (PMSI), the medical cause of Death (CepiDC), ... 
The original database is built and maintained by the French Health National Insurance Fund, called CNAM.
A [collaborative documentation of the SNDS](https://documentation-snds.health-data-hub.fr/) and an [interactive dictionary of the whole information system](https://drees.shinyapps.io/dico-snds/) can be found online.  

## Methods

We are working on a SNDS extraction involving four millions people with a ten-year depth, totalling about one billion events. We constructed event sequences including ICD10-coded diagnoses, CCAM medical procedures, ATC drug prescriptions and NABM biology procedures. The total number of separate events is 5389. The approach adopted is an implementation of word2vec discribed in *Levy et Goldberg, 2016*. The method boils down to a SVD decomposition of the PPMI (shifted positive pointwise mutual information matrix). The main computation happens when computing the events co-occurrence matrix that is used to obtain the PMI matrix :
- Calculation of the matrix of co-occurrences between events over a fixed time window (30 days).
- Vector construction by SVD on the PPMI matrix.

## Results

For each distinct event (medical concept), we obtain a vector of dimension 300 (embedding). To assess the usefulness of these representations, we use two qualitative approaches:
- Two-dimensional projection of vectors: identification of groups of events that may reflect the treatment of a pathology
- For five diseases (pyelonephritis, hip fracture, wrist sprain, diabetes, suicide attempt), calculation of the ten nearest distinct events (cosine distance) and comparison with medical knowledge.

In order to open up this rich and synthetic information from the SNDS, these vectors will be made available to the scientific community. We believe that they can be used in many approaches. A classification of these vectors will be presented and reconciled with the trees of the usual nomenclatures, which will make it possible to test their relevance. The predictive contribution of these vectors will be demonstrated in the study of hospitalizations in the elderly.

##### [-> Download the embeddings](/download/echantillon_mid_grain_r=90-centered2019-12-05_19:11:27.json)

---

#### Ressources

- [O. Levy et Y. Goldberg, « Neural Word Embedding as Implicit Matrix Factorization »](https://levyomer.files.wordpress.com/2014/09/neural-word-embeddings-as-implicit-matrix-factorization.pdf)
- [A. L. Beam et al., « Clinical Concept Embeddings Learned from Massive Sources of Multimodal Medical Data », arXiv:1804.01486 [cs, stat], avr. 2018.](http://arxiv.org/abs/1804.01486)

##### NOTE

**This information is a research work and this tool should not be used for medical purposes.**