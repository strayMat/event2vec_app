import json
import re

import pandas as pd

# Loading data
# Path to data
path2embedding_dict = 'app_data/echantillon_mid_grain_r=90-centered2019-12-05_19:11:27.json'
path2concept_infos = 'app_data/concept_infos.csv'
path2tsne_data = 'app_data/tsne_data_d=150_p=30_niter=1000.csv'
path2explanations = 'app_data/explanations.md'
DL_DIRECTORY = 'app_data'

# tsne_paramaters to display
rex = re.search('d=(\d\d*)_p=(\d\d*)_niter=(\d\d*).csv$', path2tsne_data)
embedding_dimension = rex.group(1)
perplexity = rex.group(2)
niter = rex.group(3)

#  reading data
with open(path2embedding_dict, 'r') as f:
    embeddings_dict = json.load(f)

with open(path2explanations, 'r') as f:
    explanation_markdown = f.read()

tsne_data = pd.read_csv(path2tsne_data)
embedding_df = pd.read_csv(path2concept_infos).dropna()

embedding_df['concept_embedding'] = embedding_df['concept_code'].map(lambda x: embeddings_dict[x])
df_wo_embeddings = embedding_df.loc[:, ['concept_code', 'concept_name', 'concept_terminology']]

drop_down_choices = []
for c, l, t in zip(embedding_df["concept_code"], embedding_df["concept_name"], embedding_df['concept_terminology']):
    # only allow to search for cim10, atc7 and ccam
    if t in ['cim10', 'atc7', 'ccam']:
        # Indexing the drop_down menu takes too long, so we restrain the choices
        drop_down_choices.append({'label': c + ' : ' + l, 'value': c})

available_terminologies = embedding_df['concept_terminology'].unique()

available_terminologies_choices = []
for term in available_terminologies:
    if term != 'unknown':
        available_terminologies_choices.append({'label': term, 'value': term})
