# -*- coding: utf-8 -*-
import base64
import sys
from typing import List

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from flask import Flask, send_from_directory

from dataLoader import embedding_df, DL_DIRECTORY
from functions import generate_table, concept_proximity
from tabs.explanationsTab import explanations_tab
from tabs.proximityTab import proximity_query_tab
from tabs.tsnePlotTab import tsne_plot_tab

# print(__file__)
project_home = u'/home/project/dash_app'
if project_home not in sys.path:
    sys.path = [project_home] + sys.path


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

server = Flask(__name__)

app = dash.Dash(name='my_dash_app',
                server=server,
                external_stylesheets=external_stylesheets,
                suppress_callback_exceptions=True)

app.layout = html.Div([

    # Shared header
    html.Div([
        html.Div([
            html.Img(
                id='las-logo',
                src='data:image/png;base64,{}'.format(
                    base64.b64encode(
                        open('app_data/imgs/logo-drees.png', 'rb').read()
                    ).decode()
                ),
                style={'width': '150', 'height': '150'}
            )], className='two columns', style={
            'width': '5%',
            # 'display': 'inline-block',
            'padding': "5px"}),

        html.Div([
            html.H4("Snds2vec"),
            html.P("Using word2vec on France biggest \n medico-administrative database",
                   style={"font-style": "italic"})
        ], className='two columns', style={'width': '15%'}),

        dcc.Tabs(
            id="tabs", value='tsne_plot', children=[
                dcc.Tab(label='Explanations', value='explanations'),
                dcc.Tab(label='TSNE plot', value='tsne_plot'),
                dcc.Tab(label='Proximity query', value='proximity_query')])
    ], className='row'),

    html.Div(id='tabs-content')
])


# callback functions (user interactions)
@app.callback(Output('tabs-content', 'children'),
              [Input('tabs', 'value')])
def render_content(tab):
    if tab == 'tsne_plot':
        return tsne_plot_tab
    elif tab == 'proximity_query':
        return proximity_query_tab
    elif tab == 'explanations':
        return explanations_tab


@app.callback(
    dash.dependencies.Output('output-proximity', 'children'),
    [dash.dependencies.Input('query_concept1', 'value'),
     dash.dependencies.Input('query_concept2', 'value'),
     dash.dependencies.Input('query_terminologies', 'value')])
def update_similarity(
        query_concept1: str,
        query_concept2: List[str],
        query_terminologies: List[str]):
    k = embedding_df.shape[0]
    top_k_concepts = concept_proximity(
        query_code=query_concept1,
        embeddings_df=embedding_df,
        k=k,
        target_terminologies=query_terminologies)

    if query_concept2:
        top_k_concepts = top_k_concepts.loc[top_k_concepts['concept_code'].isin(query_concept2), :]

    return generate_table(top_k_concepts, max_rows=30)

@server.route("/download/<path:path>")
def download(path):
    """Serve a file from the upload directory."""
    return send_from_directory(DL_DIRECTORY, path, as_attachment=True)

# if __name__ == '__main__':
#     app.run_server(
#         debug=True,
#         host='0.0.0.0',
#         port=8050)
# server = app.server # the Flask app to pass for flask deployment
